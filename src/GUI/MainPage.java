package GUI;

import Database.FoodCourt;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import productowner.FoodCourtModel;

public class MainPage extends javax.swing.JFrame {
    
    ArrayList<FoodCourtModel> flist;
    public MainPage() {
        initComponents();
        //Getting Foodcourts
        
        try{
            flist = new FoodCourt().GetALL();
        }catch(Exception e)
        {
               JOptionPane.showMessageDialog(this,
                 ""+e,
                 "Inane error",
                 JOptionPane.ERROR_MESSAGE);
              
        }
        
       DefaultTableModel m = (DefaultTableModel) Ftable.getModel();
       m.setRowCount(0);
       for(FoodCourtModel modelf : flist)
       {
        DefaultTableModel  model = (DefaultTableModel) Ftable.getModel();
        Object row[] = new Object[7];
      
            row[0] = modelf.getName();
            row[1] = modelf.getOwner();
            row[2] = modelf.getAddress();
            row[3] = modelf.getContact();
            row[4] = modelf.getEmail();
            row[5] = modelf.getSubEnd();
            row[6] = modelf.getId();
           
            model.addRow(row);
            
        
       }
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel_Sub = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        address = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        sub = new javax.swing.JComboBox<>();
        name = new javax.swing.JTextField();
        ownername = new javax.swing.JTextField();
        contact = new javax.swing.JTextField();
        password = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        email = new javax.swing.JLabel();
        jTextField_GameZoneEmail = new javax.swing.JTextField();
        email1 = new javax.swing.JLabel();
        waterqty = new javax.swing.JTextField();
        email2 = new javax.swing.JLabel();
        Resettime = new javax.swing.JTextField();
        email3 = new javax.swing.JLabel();
        drinkqty = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Ftable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel7.setText("Add FoodCourt");
        jPanel4.add(jLabel7);
        jLabel7.setBounds(381, 15, 320, 40);

        jLabel_Sub.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel_Sub.setText("Sub Yr");
        jPanel4.add(jLabel_Sub);
        jLabel_Sub.setBounds(556, 181, 100, 50);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setText("Owner Name");
        jPanel4.add(jLabel11);
        jLabel11.setBounds(132, 151, 220, 60);

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel12.setText("Contact No");
        jPanel4.add(jLabel12);
        jLabel12.setBounds(132, 261, 220, 60);

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel14.setText("FoodCourt Name");
        jPanel4.add(jLabel14);
        jLabel14.setBounds(132, 51, 220, 60);

        address.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(address);
        address.setBounds(552, 111, 350, 50);

        jButton1.setText("ADD");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton1);
        jButton1.setBounds(412, 471, 230, 50);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("Passsword");
        jPanel4.add(jLabel9);
        jLabel9.setBounds(132, 351, 170, 60);

        sub.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        sub.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1 yr", "2 yr", "3 yr", "4 yr", " " }));
        jPanel4.add(sub);
        sub.setBounds(661, 178, 140, 50);

        name.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(name);
        name.setBounds(132, 111, 230, 40);

        ownername.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(ownername);
        ownername.setBounds(132, 211, 230, 40);

        contact.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(contact);
        contact.setBounds(132, 311, 230, 40);

        password.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(password);
        password.setBounds(132, 401, 230, 40);

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel19.setText("Address");
        jPanel4.add(jLabel19);
        jLabel19.setBounds(552, 51, 220, 60);

        email.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        email.setText("Email ID");
        jPanel4.add(email);
        email.setBounds(560, 230, 220, 60);

        jTextField_GameZoneEmail.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(jTextField_GameZoneEmail);
        jTextField_GameZoneEmail.setBounds(560, 290, 350, 40);

        email1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        email1.setText("Water QTY");
        jPanel4.add(email1);
        email1.setBounds(780, 390, 100, 60);

        waterqty.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(waterqty);
        waterqty.setBounds(890, 400, 58, 40);

        email2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        email2.setText("Reset Time");
        jPanel4.add(email2);
        email2.setBounds(560, 340, 130, 60);

        Resettime.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(Resettime);
        Resettime.setBounds(701, 348, 191, 40);

        email3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        email3.setText("Cold Drink QTY");
        jPanel4.add(email3);
        email3.setBounds(560, 400, 167, 60);

        drinkqty.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jPanel4.add(drinkqty);
        drinkqty.setBounds(710, 400, 58, 40);

        jTabbedPane2.addTab("Add FoodCourt", jPanel4);

        Ftable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Owner Name", "Address", "Contact", "Email", "Sub End", "FoodCourt ID"
            }
        ));
        jScrollPane1.setViewportView(Ftable);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1034, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 250, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("View FoodCourts", jPanel5);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        jTabbedPane1.addTab("FoodCourt", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:Create button  Main Add new GameZoneButton

        String Fname = name.getText().trim();
        String Oname = ownername.getText().trim();
        String Email = jTextField_GameZoneEmail.getText().trim();
        String Ad = address.getText().trim();
        String C = contact.getText().trim();
        String p = password.getText().trim();
        
        String Time = Resettime.getText().trim();
         String drink    = drinkqty.getText().trim();
         String wa = waterqty.getText().trim();


        if(Fname.length() ==0 || Oname.length() ==0 || Email.length() ==0 || Ad.length() ==0 || C.length() == 0 || p.length() == 0){
            JOptionPane.showMessageDialog(this,
                "Provide proper data",
                "Inane error",
                JOptionPane.ERROR_MESSAGE);
            return;

        }

        String select = sub.getSelectedItem().toString();
        int subyears = Integer.parseInt(select.substring(0,1));

        long millis=System.currentTimeMillis();
        java.sql.Date currentdate=new java.sql.Date(millis);
        String currentdateString = currentdate.toString();
        String TempStr = currentdateString.substring(4);
        //2015-03-30 format is this
        int currentyr = Integer.parseInt(currentdateString.substring(0,4));
        int Endsubyr = currentyr + subyears;

        String FinalEndsubString = Endsubyr+TempStr;
        java.sql.Date finalEndsubDate   =  java.sql.Date.valueOf(FinalEndsubString);

        FoodCourtModel model = new FoodCourtModel(0, Fname,  Ad, Oname,C, finalEndsubDate, Email );
  
        
        FoodCourt data = new FoodCourt();
        try{
            data.AddFoodCourt(model,p);
            JOptionPane.showMessageDialog(this,
                "FoodCourt Added successfully",
                "Inane error",
                JOptionPane.PLAIN_MESSAGE);
            return;
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(this,
                ""+e,
                "Inane error",
                JOptionPane.ERROR_MESSAGE);

        }

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Ftable;
    private javax.swing.JTextField Resettime;
    private javax.swing.JTextField address;
    private javax.swing.JTextField contact;
    private javax.swing.JTextField drinkqty;
    private javax.swing.JLabel email;
    private javax.swing.JLabel email1;
    private javax.swing.JLabel email2;
    private javax.swing.JLabel email3;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel_Sub;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTextField jTextField_GameZoneEmail;
    private javax.swing.JTextField name;
    private javax.swing.JTextField ownername;
    private javax.swing.JTextField password;
    private javax.swing.JComboBox<String> sub;
    private javax.swing.JTextField waterqty;
    // End of variables declaration//GEN-END:variables
}

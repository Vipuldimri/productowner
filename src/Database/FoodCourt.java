
package Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import productowner.FoodCourtModel;

/**
 *
 * @author dimri
 */
public class FoodCourt {
 
    Connection conn;
    public FoodCourt()
    {
           conn = Connect.getconnection();
    }
    
     public void AddFoodCourt(FoodCourtModel model, String password) throws Exception
    {
      
            final String AddGameZone = "insert  into FoodCourts(Name,Address,Owner,Contact,SubEnd,Email) VALUES(?,?,?,?,?,?)";            
            PreparedStatement pstmt = conn.prepareStatement(AddGameZone);
            pstmt.setString(1,model.getName());
            
            pstmt.setString(2, model.getAddress());
            
            pstmt.setString(3, model.getOwner());
            
            pstmt.setString(4, model.getContact());
            
            pstmt.setDate(5, new java.sql.Date(model.getSubEnd().getTime()));
           
            pstmt.setString(6, model.getEmail());
           
            pstmt.executeUpdate();
            

              String Catetable = "create table "+model.getName()+"_Category\n" +
                "(\n" +
                "  ID    int auto_increment\n" +
                "    primary key,\n" +
                "  Name  varchar(30) default '' not null,\n" +
                "  Image longblob               not null,\n" +
                "  constraint Demo_Category_ID_uindex\n" +
                "  unique (ID)\n" +
                ");";

  
              System.out.println(Catetable);
              Statement statement = conn.createStatement() ;
              statement.execute(Catetable);
              
         
              
          
              
              
              
              String Colltable ="create table "+model.getName()+"_collection\n" +
"(\n" +
"  Collect varchar(500) charset utf8 null,\n" +
"  date    date                      null\n" +
");\n" +
"";

            
               statement = conn.createStatement() ;
               statement.execute(Colltable);
            
              
              
                    
              
             
              String Itemtable ="create table "+model.getName()+"_Items\n" +
"(\n" +
"  ID       int auto_increment\n" +
"    primary key,\n" +
"  Name     varchar(50) null,\n" +
"  Price    varchar(50) null,\n" +
"  Category varchar(50) null,\n" +
"  QTY      int         null,\n" +
"  constraint Demo_Items_ID_uindex\n" +
"  unique (ID)\n" +
");";



              
              
     
              Statement stmt222 = conn.createStatement() ;
              stmt222.execute(Itemtable);
       
              
              
String Token ="create table "+model.getName()+"_TokenCount\n" +
"(\n" +
"  Token int not null\n" +
"    primary key\n" +
");";

    
              
     
              Statement stmt91 = conn.createStatement() ;
              stmt91.execute(Token);
       
              
              
              
                    
        String insertToken = "INSERT into "+model.getName()+"_TokenCount(Token) values (?);";
        PreparedStatement pstmt91 = conn.prepareStatement(insertToken);
        pstmt91.setInt(1,0);
        pstmt91.executeUpdate();
        
              
              
              
              
              
              
              
              
              
              String usertable ="create table "+model.getName()+"_Users\n" +
"(\n" +
"  ID       int auto_increment\n" +
"    primary key,\n" +
"  Name     varchar(50) null,\n" +
"  Phone    varchar(50) null,\n" +
"  Address  varchar(50) null,\n" +
"  Email    varchar(50) null,\n" +
"  UserName varchar(50) null,\n" +
"  Password varchar(50) null,\n" +
"  type     varchar(20) null,\n" +
"  constraint Demo_Users_ID_uindex\n" +
"  unique (ID)\n" +
");\n" +
"";


              
              
              Statement stmt2222 = conn.createStatement() ;
              stmt2222.execute(usertable);

              
        
        String maxid = "select max(Id) from FoodCourts;";
        int id=0;
       
      
                   Statement stmt=conn.createStatement();  
                   ResultSet rs = stmt.executeQuery(maxid);
                   while(rs.next())  
                   {
                    id = rs.getInt(1);
                   
                   }
                   
        
        
        
        
        String insertuserdetails = "INSERT into "+model.getName()+"_Users(Name,Phone,Address,Email,UserName,Password,Type) values (?,?,?,?,?,?,?);";
        
        PreparedStatement pstmt2 = conn.prepareStatement(insertuserdetails);
        pstmt2.setString(1,model.getOwner());
        pstmt2.setString(2, model.getContact());
        pstmt2.setString(3, model.getAddress());
        pstmt2.setString(4, model.getEmail());
        pstmt2.setString(5, model.getOwner());
        pstmt2.setString(6, password);
        pstmt2.setString(7, "admin");
        pstmt2.executeUpdate();
        
        
              
              
       }
     public ArrayList<FoodCourtModel> GetALL() throws Exception
     {
           ArrayList<FoodCourtModel> list = new ArrayList<>();
           String query = "Select * from FoodCourts";
      
                   Statement stmt=conn.createStatement();  
                   ResultSet rs = stmt.executeQuery(query);
                   while(rs.next())  
                   {
                    FoodCourtModel item = new FoodCourtModel(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getDate(6),rs.getString(7));
                    list.add(item);
                    
                   }
                   
        
        return list;
     }
    
}

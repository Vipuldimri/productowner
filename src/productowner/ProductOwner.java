/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productowner;

import Database.Connect;
import GUI.MainPage;

/**
 *
 * @author dimri
 */
public class ProductOwner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try 
        {
             Connect connection = new Connect();
              MainPage main = new MainPage();
              main.setVisible(true);
                
       
        } catch (Exception ex) 
        {
            System.out.println("Error Unable to connect to server : "+ex);
            System.exit(0);
        }

    }
    
}
